import { Cookies } from "react-cookie";
import { Navigate } from "react-router-dom";

const baseURl = "/api"; // TODDO: change this 

 
async function login(userData){
    let response, jsonResponse;
    try{
        response = await fetch(`${baseURl}/user/login`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userData)
        });
        
        jsonResponse = await response.json();
    } catch (error){
        return error
    }
    
    return jsonResponse;
}

async function getPrograms(){
    let response, jsonResponse;
    try{
        response = await fetch(`${baseURl}/datamodel/programs`);
        jsonResponse = await response.json();
     
        jsonResponse = jsonResponse.map(program => {
            program.id = program.pk
            return program 
        })
    } catch (error){
        return error
    }
    
    return jsonResponse;
}


async function uploadFile(data){
    let response, jsonResponse;
    const fd = new FormData();
    fd.append("uploaded_file", data.file);
    fd.append("data_type", data.dataType);
    fd.append("program", data.program.name);

    try{
        response = await fetch(`${baseURl}/datamodel/uploaddata`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                // 'Content-Type': 'application/json',
            },
            body: fd//JSON.stringify(userData)
        });

        if (response.status === 200){
            jsonResponse = await response.json();
        }else{
            jsonResponse = {
                error: {
                    message: "Couldn't upload the file!"
                }
            }
        }
        
        
    } catch (error){
        return error
    }
    
    return jsonResponse;
}


async function getProfile(){
    let response, jsonResponse;
    try{
        response = await fetch(`${baseURl}/user/profile`);
        jsonResponse = await response.json();
        jsonResponse = jsonResponse.user;
    } catch (error){
        return error
    }
    
    return jsonResponse;
}


async function logout(){
    let response, jsonResponse;
    try{
        response = await fetch(`${baseURl}/user/logout`);
        jsonResponse = await response.json();
        if (response.status === 200){
            document.location.reload();
        }
    } catch (error){
        return error
    }
    
    return jsonResponse;
}


export {
    login,
    logout,
    uploadFile,
    getPrograms,
    getProfile,
}
