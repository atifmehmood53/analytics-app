import React, {useState} from "react";
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import { login } from "../api"


function LoginPage() {
    let navigate = useNavigate();
    const [cookies , setCookies] = useCookies(['sessionid'])
    const [error, setError] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    async function onSubmit(e){
        e.preventDefault();
        let response = await login({ username, password });

        if (response.error){
            setError(response.error.message);
        }else{
            setError("");
            const session = await window.cookieStore.get("sessionid");
            setCookies(session.name, session.value, {...session, expires : new Date(session.expires)});
            //window.location = "http://localhost:10000/admin";
        }
    }

    return (
        <Row className="align-content-center justify-content-center min-vh-100">
            <Col lg={6}>
                <Form onSubmit={onSubmit}>
                    <p className="text-danger">{error}</p>
                    <Form.Group className="mb-3" controlId="username">
                        <Form.Label>Username</Form.Label>
                        <Form.Control 
                            required
                            type="text" 
                            placeholder="Enter username" 
                            value={username}  
                            onChange={(e) => setUsername(e.target.value)}
                        />
                        <Form.Text className="text-muted">
                            Username that was assigned by the admin.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            required
                            type="password" 
                            placeholder="Password" 
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Login
                    </Button>
                </Form>
            </Col>
        </Row>
    );
  }
  

export default LoginPage;