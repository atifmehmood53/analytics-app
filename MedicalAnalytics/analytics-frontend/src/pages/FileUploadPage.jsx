import React, {useState, useEffect} from "react";
import { Form, Button, Row, Col } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { getPrograms, uploadFile } from "../api";

export default function FileuploadPage(){
    const [file, setFile] = useState();
    const [program, setProgram] = useState();
    const [dataType, setDataType] = useState('PATIENT_ROASTER');
    const [availablePrograms, setAvailablePrograms] = useState([]);

    useEffect(() => {
        (async () => {
            const programResponse = await getPrograms();
            setAvailablePrograms(programResponse)
            setProgram(programResponse.length && programResponse[0]);
        })()
    }, []);

    function onChangeProgram(e){
        const selectedProgram = availablePrograms.find((prog)=>prog.id == e.target.value);
        setProgram(selectedProgram);
    }


    async function onSubmit(e){
        e.preventDefault();
        const response = await uploadFile({
            file,
            dataType,
            program
        })

        if (response.error){
            toast.error(response.error.message, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }else{
            toast.success("Data uploaded sucessfully!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    }


    return (
        <Row className="align-content-center justify-content-center min-vh-100">
            <Col lg={6}>
                <Form  onSubmit={onSubmit}>
                <Form.Group controlId="csvFile" className="mb-3">
                    <Form.Label>Please upload a CSV file</Form.Label>
                    <Form.Control type="file" accept=".csv" onChange={(e) => setFile(e.target.files.length && e.target.files[0])} required/>
                </Form.Group>
                <Form.Group controlId="dataType" className="mb-3">
                    <Form.Label>Data Type</Form.Label>
                    <Form.Select aria-label="data type" value={dataType} onChange={(e) => setDataType(e.target.value)} required>
                        <option value="A">Type A</option>
                        <option value="PATIENT_ROASTER">PATIENT ROASTER</option>
                    </Form.Select>
                </Form.Group>
                <Form.Group controlId="csvFile" className="mb-3">
                    <Form.Label>Program</Form.Label>
                    <Form.Select aria-label="Programs" value={program && program.id} onChange={onChangeProgram} required>
                        {availablePrograms.map((prog)=> <option key={prog.id} value={prog.id}>{prog.name}</option>)}
                    </Form.Select>
                </Form.Group>
                <Button type="sumbit" >Upload</Button>
                </Form>
            </Col>
        </Row>
    )
}