import { useEffect, useState } from 'react';
import logo from './logo.svg';
import LoginPage from "./pages/LoginPage";
import FileUploadPage from "./pages/FileUploadPage";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { CookiesProvider, useCookies } from "react-cookie";
import { ToastContainer, toast } from 'react-toastify';
import { Navbar, Nav, Container } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import { getProfile, logout } from "./api"
import './App.css';

function App() {
  // get session and see if the sesion is expred or not 
  const [cookies, setCookie] = useCookies(['sessionid', "csrftoken"]);
  const [user, setUser] = useState(null);

  useEffect(()=>{
    if (cookies.sessionid){
      (async ()=>{
        setUser(await getProfile())
      })()
    }else{
      setUser(null);
    }
  }, [cookies.sessionid])

  useEffect(()=>{
    console.log(user)
  })
  
  return (
    <>
    <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand >Placeholder Text</Navbar.Brand>
          <Navbar.Toggle />
          {user?.isSuperUser && <Nav className="me-auto">
            <Nav.Link href="http://localhost:10000/admin">Admin Site</Nav.Link>
          </Nav>}
          <Navbar.Collapse className="justify-content-end">
            {user?.username && <Navbar.Text>
              Signed in as: <a>{user.username}</a> <a className='text-muted' onClick={logout}>Logout</a>
            </Navbar.Text>}
          </Navbar.Collapse>
        </Container>
    </Navbar> 
    <BrowserRouter>
      <Routes>
        { !cookies.sessionid ? <Route path="/" 
          element={
            <Container>
              <LoginPage/>
            </Container>
          } 
        />
        : <Route path="/" element={
            <>
            {user && <Container>
              {user?.permissions.find((perm)=> perm === "UserManager.can_upload_csv") || user?.isSuperUser ? <FileUploadPage /> : <h5 className='mt-5'>You Don't have File Upload Permission</h5>}
            </Container>}
            </>
          } />
        }
      </Routes>
    </BrowserRouter>
    <ToastContainer />
    </>
  );
}

export default App;
