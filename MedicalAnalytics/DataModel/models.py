from django.db import models
import datetime
from dateutil import parser
from dateutil.relativedelta import relativedelta

# Create your models here.
class Programs(models.Model):
    class Meta:
        db_table = "Programs"
    name = models.CharField(max_length = 10, unique=True)
    exec_sponsor = models.CharField(max_length = 200)
    partners = models.CharField(max_length = 200)
    expected_length_of_stay = models.IntegerField(default = 0)
    effective_date = models.DateField()
    status = models.CharField(max_length = 10)


class PatientRoaster(models.Model):
    class Meta:
        db_table = "PatientRoaster"
    patient_id = models.CharField(max_length = 200, unique = True)
    program = models.ForeignKey(Programs, on_delete=models.CASCADE, null=False)
    gender = models.CharField(max_length = 10)
    date_of_birth = models.DateField()
    primary_diagnosis = models.CharField(max_length = 200)
    postal_code = models.CharField(max_length = 200)
    family_doctor_attachment = models.CharField(max_length = 1)
    family_doctor_name = models.CharField(max_length = 200)
    family_doctor_location = models.CharField(max_length = 200)
    referral_date = models.DateField()
    referral_source_type = models.CharField(max_length = 200)
    referral_source_name = models.CharField(max_length = 200)
    referral_source_outcome = models.CharField(max_length = 200)
    enrollment_date = models.DateField()
    home_visit_date = models.DateField()
    discharge_date = models.DateField()
    discharge_reason = models.CharField(max_length = 200)
    length_of_stay = models.IntegerField()


    def compute_length_of_stay(self):
        print(self.enrollment_date)
        print(self.discharge_date)
        start = datetime.datetime.strptime(self.enrollment_date, '%Y-%m-%d')
        end = datetime.datetime.strptime(self.discharge_date, '%Y-%m-%d')    
        lengthOfStay = end - start

        return lengthOfStay.days


    def save(self, *args, **kwargs):
        self.length_of_stay = self.compute_length_of_stay()
        super(PatientRoaster, self).save(*args, **kwargs)