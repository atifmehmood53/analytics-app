import json
from django.apps import AppConfig

class DatamodelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'DataModel'
    
    # def ready(self):
    #     from . import scripts
    #     print("asd")
    #     scripts.upload_programs_to_db()