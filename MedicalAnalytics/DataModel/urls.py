from django.urls import path
from .views import ProgramView, UploadFileView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('programs', ProgramView.as_view(), name='get_programs'),
    path("uploaddata", csrf_exempt(UploadFileView.as_view()), name="upload_data")
]