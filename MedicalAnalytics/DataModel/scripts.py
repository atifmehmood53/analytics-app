import json
from .models import Programs

def upload_programs_to_db():
    try:
        with open('./data/programs.json', 'r') as f:
            programs_json = json.load(f)["programs"]
        
        for program in programs_json:
            Programs(name = program["name"],
                    exec_sponsor = program["exec_sponsor"],
                    partners = program["partners"],
                    expected_length_of_stay = program["expected_length_of_stay"],
                    effective_date = program["effective_date"],
                    status = program["status"]).save()

    except Exception as e:
         print("Oops!", e.__class__, "occurred.")