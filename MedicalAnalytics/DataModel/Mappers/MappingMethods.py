import datetime

class MappingMethods:

    def __init__(self):
        pass

    def lengthOfStay(self, start, end):
        start = datetime.datetime.strptime(start, '%Y-%m-%d')
        end = datetime.datetime.strptime(end, '%Y-%m-%d')    
        lengthOfStay = end - start

        return lengthOfStay.days

    def identity(self, value):
        return value