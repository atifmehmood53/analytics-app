import json
import os
import csv
from ..models import PatientRoaster, Programs
from UtilEnums import DataType
from django.core.serializers import serialize
from django.conf import settings

class MapperService:
    structure_json_path = "DataModel/Mappers/MappingStructure.json"

    def __init__(self):
        self.mapping_structure = json.load(open(os.path.join(settings.BASE_DIR, self.structure_json_path)))
        
    def map_csv_to_db(self, file, data_type, program):

        file_contents = self.__read_file(file)
        processed_raw_data =  self.__prepare_source_data(file_contents)

        if (data_type == DataType.PATIENT_ROASTER.value):
            return self.__map_patient_to_table(processed_raw_data, program)

        return False


    def __map_patient_to_table(self, processed_data, program):

        for each_row in processed_data:
            patient = PatientRoaster()
            for each_col in each_row.keys():
                source_col = each_col
                dest_col = self.mapping_structure[source_col]["target_field"]
                patient.__setattr__(dest_col, each_row[source_col])
            patient.__setattr__("program", Programs.objects.get(name=program))
            patient.save()

        return "patient"

    def __prepare_source_data(self, file_contents):
        
        source_data = []
        headers = [i.strip(" ") for i in file_contents[0]]

        for record_index in range(1, len(file_contents)-1):
            data_dict = {}
            for val_index in range(0, len(file_contents[record_index])):
                data_dict[headers[val_index]] = file_contents[record_index][val_index]
            
            source_data.append(data_dict)

        return source_data

    def __read_file(self, csv_file):

        parsed_file = csv_file.read().decode("utf-8")
        parsed_file = parsed_file.split("\n")

        for each_line in range(0, len(parsed_file)):
            parsed_file[each_line] = parsed_file[each_line].split(",")

        return parsed_file