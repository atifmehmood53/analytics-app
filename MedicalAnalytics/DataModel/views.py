from django.shortcuts import render
from django.http import HttpResponse
from .Program.ProgramService import ProgramService
from .Mappers.MapperService import MapperService
from django.core.serializers import serialize
import json
import os
from django.conf import settings
from django.views import View    

class ProgramView(View):
    program_service = ProgramService()

    def get(self, request):
        
        all_programs = self.program_service.get_list_programs()
        return HttpResponse(all_programs, content_type="application/json")

class UploadFileView(View):
    mapper_service = MapperService()

    def post(self, request):

        self.__request_validation(request)

        result = self.mapper_service.map_csv_to_db(request.FILES['uploaded_file'], request.POST["data_type"], request.POST["program"])
        return HttpResponse(result, content_type="application/json")

    def __request_validation(self, request):

        csv_file = request.FILES.get("uploaded_file", None)
        data_type = request.POST.get("data_type", None)
        program = request.POST.get("program", None)

        if (csv_file == None or csv_file.content_type != "text/csv"):
            raise FileNotFoundError("File uploaded must be csv")

        if (data_type == None):
            raise ValueError("Data type must be specified")

        if (program == None):
            raise ValueError("Program must be specified")
