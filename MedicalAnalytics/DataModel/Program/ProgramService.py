from ..models import Programs
import json
from django.core.serializers import serialize

class ProgramService:

    def __init__(self):
        pass

    def get_list_programs(self):
        
        all_programs = []

        programs_fetched = Programs.objects.all()
        serialized = json.loads(serialize("json", programs_fetched))

        for each_program in serialized:
            each_program["fields"]["pk"] = each_program["pk"]
            all_programs.append(each_program["fields"])
            
        return json.dumps(all_programs)