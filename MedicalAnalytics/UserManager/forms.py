from django import forms 

class Login_Form(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}),label='ID')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class' : 'form-control'}),label='Password')