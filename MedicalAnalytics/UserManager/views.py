from django.contrib.auth import authenticate, login, logout 
from django.contrib.auth.models import Permission
from django.http import HttpResponse
from django.shortcuts import render, reverse , redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.contrib.auth.decorators import login_required

# Create your views here.
@csrf_exempt
@api_view(['POST'])
def login_view(request):
    if request.user.is_authenticated:
        return request.user.get_profile();

    if request.method == 'POST':
        username = request.data['username']
        password = request.data['password']
        user = authenticate(request,username= username, password= password)
       
        if (user!=None):
            login(request,user)
            # if 'next' in request.POST:
            #     #some way to send invalid details back
            #     return redirect(request.POST.get('next'))
            return Response({"user":user.get_profile()})
        else:
            error = "Invalid username or password."
            
    return Response({"error" : { 'message': error }}, status=401)



@login_required()
@api_view(['GET'])
def profile(request):
    #profile
    user = request.user
    return Response({"user":user.get_profile()})


@api_view(['GET'])
def logout_view(request):
    logout(request)
    return Response({"success": True})