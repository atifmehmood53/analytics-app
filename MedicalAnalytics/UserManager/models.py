from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

class User(AbstractUser):
    class Meta:
        permissions = [
            ("can_upload_csv", "Can Upload CSV/Data"),
        ]

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    def get_profile(self):
        profile = {
            "isSuperUser": self.is_superuser,
            "username": self.username,
            "firstName": self.first_name,
            "lastName": self.last_name,
            "fullName": self.full_name,
            "permissions" : self.get_all_permissions()
        }

        return profile
