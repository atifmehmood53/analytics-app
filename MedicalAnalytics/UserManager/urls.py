from django.contrib import admin
from django.urls import path
from . import views 
 
urlpatterns = [
    #path('',views.redirectView),
    path('login',views.login_view,name='loginView'),
    path('logout',views.logout_view,name='logoutView'),
    path('profile',views.profile,name='profile')
]